import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:cryptox/domain/entities/crypto_historical_price.dart';
import 'package:cryptox/services/repository/api/api_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

part 'home_page_event.dart';
part 'home_page_state.dart';

class HomePageBloc extends Bloc<HomePageEvent, HomePageState> {
  final apiRepository = ApiRepository();
  HomePageBloc() : super(HomePageInitial()) {
    on<HomePageEvent>((event, emit) async{
      if(event is HomePageStartEvent){
        emit(HomePageLoadingState());
        final apiResult = await apiRepository.getExchangeRateHistorical(base: "BTC", quote: "USD");
        List<CryptoHistoricalPrice> cryptoHistoricalPriceList = CryptoHistoricalPriceList.fromJson(apiResult).cryptoHistoricalPriceList;

        emit(HomePageLoadedState(cryptoHistoricalPriceList: cryptoHistoricalPriceList ));
      }
      if(event is UpdateHistoricalChartEvent){
        List<String> currencyPairList = event.currencyPair.split('/');
        final apiResult = await apiRepository.getExchangeRateHistorical(base: currencyPairList[0], quote: currencyPairList[1]);
        List<CryptoHistoricalPrice> cryptoHistoricalPriceList = CryptoHistoricalPriceList.fromJson(apiResult).cryptoHistoricalPriceList;

        emit(HomePageLoadingState());
        await Future.delayed(const Duration(milliseconds: 1000));
        emit(HomePageLoadedState(cryptoHistoricalPriceList: cryptoHistoricalPriceList));

      }


    });
  }
}
