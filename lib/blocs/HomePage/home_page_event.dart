part of 'home_page_bloc.dart';

abstract class HomePageEvent extends Equatable {
  const HomePageEvent();
}

class HomePageStartEvent extends HomePageEvent{
  final String currencyPair;
  const HomePageStartEvent({required this.currencyPair});
  @override
  List<Object?> get props => [currencyPair];
}

class UpdateHistoricalChartEvent extends HomePageEvent{
  final String currencyPair;
  const UpdateHistoricalChartEvent({required this.currencyPair});
  @override
  List<Object?> get props => [currencyPair];
}



