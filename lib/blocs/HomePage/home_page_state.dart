part of 'home_page_bloc.dart';

abstract class HomePageState extends Equatable {
  const HomePageState();
}

class HomePageInitial extends HomePageState {
  @override
  List<Object> get props => [];
}

class HomePageLoadedState extends HomePageState{
  final List<CryptoHistoricalPrice> cryptoHistoricalPriceList;
  const HomePageLoadedState({required this.cryptoHistoricalPriceList});
  @override
  List<Object?> get props => [cryptoHistoricalPriceList];
}

class HomePageLoadingState extends HomePageState{
  @override
  List<Object?> get props => [];
}
