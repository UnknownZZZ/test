import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:cryptox/services/repository/api/api_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:cryptox/config/constants.dart' as constants;
import 'package:web_socket_channel/status.dart' as status;
part 'crypto_exchange_subscription_box_event.dart';
part 'crypto_exchange_subscription_box_state.dart';

class CryptoExchangeSubscriptionBoxBloc extends Bloc<CryptoExchangeSubscriptionBoxEvent, CryptoExchangeSubscriptionBoxState> {

  final apiRepository = ApiRepository();
  CryptoExchangeSubscriptionBoxBloc() : super(CryptoExchangeSubscriptionBoxInitial()) {
    on<CryptoExchangeSubscriptionBoxEvent>((event, emit) async{



      if(event is CryptoExchangeSubscriptionBoxStartEvent){
        emit(CryptoExchangeSubscriptionBoxLoadingState());
        WebSocketChannel webSocketChannel = IOWebSocketChannel.connect(Uri.parse(constants.socketBaseURL));

        final apiResult = await apiRepository.getExchangeRate(base: 'BTC', quote: 'USD');

        await Future.delayed(const Duration(milliseconds: 1000));
        emit(CryptoExchangeSubscriptionBoxLoadedState(currencyPair: event.currencyPair, lastUpdatedTime: DateFormat().add_jm().format(DateTime.now()).toString(), latestPrice: apiResult,webSocketChannel: webSocketChannel));
      }
      if(event is CryptoExchangeSubscriptionBoxPairSelectedEvent){

        event.webSocketChannel.sink.close();
        List<String> currencyPairList = event.currencyPair.split('/');
        final apiResult = await apiRepository.getExchangeRate(base: currencyPairList[0], quote: currencyPairList[1]);

        emit(CryptoExchangeSubscriptionBoxLoadedState(currencyPair: event.currencyPair,lastUpdatedTime: DateFormat().add_jm().format(DateTime.now()).toString(), latestPrice: apiResult,webSocketChannel: event.webSocketChannel));

      }

      if(event is SubscribeToPairEvent){
        List<String> currencyPairList = event.currencyPair.split('/');
        WebSocketChannel webSocketChannel = IOWebSocketChannel.connect(Uri.parse(constants.socketBaseURL));
        final apiResult = await apiRepository.liveSubscription(base: currencyPairList[0], quote: currencyPairList[1],webSocketChannel: webSocketChannel);

        emit(CryptoExchangeSubscriptionBoxLoadedState(currencyPair: event.currencyPair,lastUpdatedTime: DateFormat().add_jm().format(DateTime.now()).toString(), latestPrice: 0,webSocketChannel: webSocketChannel));


      }

    });
  }
}
