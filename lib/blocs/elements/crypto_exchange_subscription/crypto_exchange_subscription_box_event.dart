part of 'crypto_exchange_subscription_box_bloc.dart';

abstract class CryptoExchangeSubscriptionBoxEvent extends Equatable {
  const CryptoExchangeSubscriptionBoxEvent();
}

class CryptoExchangeSubscriptionBoxStartEvent extends CryptoExchangeSubscriptionBoxEvent{
  final String currencyPair;
  const CryptoExchangeSubscriptionBoxStartEvent({required this.currencyPair});
  @override
  List<Object?> get props => [currencyPair];
}

class CryptoExchangeSubscriptionBoxPairSelectedEvent extends CryptoExchangeSubscriptionBoxEvent{
  final WebSocketChannel webSocketChannel;
  final String currencyPair;
  const CryptoExchangeSubscriptionBoxPairSelectedEvent({required this.currencyPair, required this.webSocketChannel});
  @override
  List<Object?> get props => [currencyPair,webSocketChannel];
}

class SubscribeToPairEvent extends CryptoExchangeSubscriptionBoxEvent{
   final WebSocketChannel webSocketChannel;
  final String currencyPair;
  SubscribeToPairEvent({required this.currencyPair, required this.webSocketChannel});
  @override
  List<Object?> get props => [currencyPair,webSocketChannel];
}
