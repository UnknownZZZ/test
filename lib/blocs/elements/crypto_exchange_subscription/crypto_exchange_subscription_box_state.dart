part of 'crypto_exchange_subscription_box_bloc.dart';

abstract class CryptoExchangeSubscriptionBoxState extends Equatable {
  const CryptoExchangeSubscriptionBoxState();
}

class CryptoExchangeSubscriptionBoxInitial extends CryptoExchangeSubscriptionBoxState {
  @override
  List<Object> get props => [];
}

class CryptoExchangeSubscriptionBoxLoadedState extends CryptoExchangeSubscriptionBoxState{
  WebSocketChannel webSocketChannel;
  String currencyPair;
  double latestPrice;
  String lastUpdatedTime;
  CryptoExchangeSubscriptionBoxLoadedState({required this.currencyPair, required this.lastUpdatedTime, required this.latestPrice,required this.webSocketChannel});
  @override
  List<Object?> get props => [currencyPair,lastUpdatedTime,latestPrice,webSocketChannel];
}


class CryptoExchangeSubscriptionBoxLoadingState extends CryptoExchangeSubscriptionBoxState{
  @override
  List<Object?> get props => [];
}
