import 'package:flutter/material.dart';



final kDarkTheme = ThemeData.dark().copyWith(

);

final kLightTheme = ThemeData.light().copyWith(
  textTheme:  const TextTheme(
    headline3: TextStyle(
        fontSize: 18,
        color: Color(0xFF000000),
    ),
    headline4: TextStyle(
      fontSize: 16,
      color: Color(0xFF000000),
    ),
    headline5: TextStyle(
      fontSize: 14,
      color: Color(0xFF000000),
    ),
    headline6: TextStyle(
      fontSize: 12,
      color: Color(0xFF000000),
    ),

  ),
);