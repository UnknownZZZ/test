import 'package:intl/intl.dart';

class CryptoHistoricalPrice {
  final DateTime time;
  final double Price;

  CryptoHistoricalPrice(this.time, this.Price);
}

class CryptoHistoricalPriceList{
  final List<CryptoHistoricalPrice> cryptoHistoricalPriceList;
  CryptoHistoricalPriceList({required this.cryptoHistoricalPriceList});

  factory CryptoHistoricalPriceList.fromJson(List<dynamic> json){

    List<CryptoHistoricalPrice> _cryptoHistoricalPriceList = [];
    for (var element in json) {
      //print(element['time_close']);
      //print();
      _cryptoHistoricalPriceList.add(CryptoHistoricalPrice(DateTime.parse(element['time_close']), double.parse(element['rate_close'].toString())),);
    }

    return CryptoHistoricalPriceList(
        cryptoHistoricalPriceList: _cryptoHistoricalPriceList
    );
  }

}