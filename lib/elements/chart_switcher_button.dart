import 'package:flutter/material.dart';

class ChartSwitcherButton extends StatelessWidget {
  String title;
  bool active;
  ChartSwitcherButton({Key? key, required this.title, required this.active}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 5, right: 5),
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: (active) ? Color(0xff0e263e)  : Color(0xfff1f1f1),
        ),
        padding: EdgeInsets.all(5),
        child: Text(title, style: TextStyle(color: (active) ? Color(0xffffffff)  : Color(0xff0e263e)),),
      ),
    );
  }
}
