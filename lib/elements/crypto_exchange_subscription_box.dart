import 'dart:convert';

import 'package:cryptox/blocs/HomePage/home_page_bloc.dart';
import 'package:cryptox/blocs/elements/crypto_exchange_subscription/crypto_exchange_subscription_box_bloc.dart';
import 'package:cryptox/elements/chart_switcher_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:cryptox/config/constants.dart' as constants;

class CryptoExchangeSubscriptionBox extends StatelessWidget {

  CryptoExchangeSubscriptionBox({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,

      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: const Color(0xFFe3f6fd),
      ),
      child: Column(
        children: [
          Row(

            children:  [

              BlocBuilder<CryptoExchangeSubscriptionBoxBloc, CryptoExchangeSubscriptionBoxState>(
                builder: (context, state){
                  if(state is CryptoExchangeSubscriptionBoxLoadedState){
                    return DropdownButton<String>(
                      value: state.currencyPair,
                      icon: const Icon(Icons.keyboard_arrow_down),
                      elevation: 16,
                      style: const TextStyle(color: Color(0xff0e263e)),

                      onChanged: (String? newValue) {

                        context.read<CryptoExchangeSubscriptionBoxBloc>().add(CryptoExchangeSubscriptionBoxPairSelectedEvent(currencyPair: newValue!,webSocketChannel: state.webSocketChannel));
                        context.read<HomePageBloc>().add(UpdateHistoricalChartEvent(currencyPair: newValue));
                      },
                      items: <String>['BTC/USD', 'ETH/USD', 'BNB/USD', 'XRP/USD','DOGE/USD']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    );
                  }
                  return const SizedBox(
                    child: Text('Loading ...'),
                  );
                },
              ),
              const SizedBox(width: 15,),
              BlocBuilder<CryptoExchangeSubscriptionBoxBloc, CryptoExchangeSubscriptionBoxState>(
                builder: (context,state){
                  if(state is CryptoExchangeSubscriptionBoxLoadedState){
                    return Expanded(
                      child: GestureDetector(
                        onTap: (){

                          context.read<CryptoExchangeSubscriptionBoxBloc>().add(SubscribeToPairEvent(currencyPair: state.currencyPair, webSocketChannel: state.webSocketChannel));
                        },
                        child: ChartSwitcherButton(
                          title: 'Subscribe',
                          active: true,
                        ),
                      ),
                    );
                  }
                  return ChartSwitcherButton(
                    title: 'Subscribe',
                    active: false,
                  );

                },
              ),

            ],
          ),
          const SizedBox(height: 15,),
          Row(
            children: [
              BlocBuilder<CryptoExchangeSubscriptionBoxBloc, CryptoExchangeSubscriptionBoxState>(
                  builder: (context, state){
                    if(state is CryptoExchangeSubscriptionBoxLoadedState){
                      return StreamBuilder(

                          stream: state.webSocketChannel.stream,
                          builder: (context, snapshot){

                            if(snapshot.hasData){

                              if(snapshot.connectionState == ConnectionState.active){
                                String takerSide = jsonDecode(snapshot.data.toString())['taker_side'];
                                if(takerSide == 'BUY'){
                                  return Text((NumberFormat.currency(locale: 'eu', symbol:'\$' ,decimalDigits: 3, customPattern: '\u00a4 #,###.#').format(jsonDecode(snapshot.data.toString())['price'])), style: Theme.of(context).textTheme.headline5?.merge(TextStyle(color: Colors.red )),);
                                }
                                if(takerSide == 'SELL'){
                                  return Text((NumberFormat.currency(locale: 'eu', symbol:'\$' ,decimalDigits: 3, customPattern: '\u00a4 #,###.#').format(jsonDecode(snapshot.data.toString())['price'])), style: Theme.of(context).textTheme.headline5?.merge(TextStyle(color: Colors.green )),);
                                }
                                if(takerSide == 'BUY_ESTIMATED'){
                                  return Text((NumberFormat.currency(locale: 'eu', symbol:'\$' ,decimalDigits: 3, customPattern: '\u00a4 #,###.#').format(jsonDecode(snapshot.data.toString())['price'])), style: Theme.of(context).textTheme.headline5?.merge(TextStyle(color: Colors.red )),);
                                }
                                if(takerSide == 'SELL_ESTIMATED'){
                                  return Text((NumberFormat.currency(locale: 'eu', symbol:'\$' ,decimalDigits: 3, customPattern: '\u00a4 #,###.#').format(jsonDecode(snapshot.data.toString())['price'])), style: Theme.of(context).textTheme.headline5?.merge(TextStyle(color: Colors.green )),);
                                }
                              }
                            }
                            return Text((NumberFormat.currency(locale: 'eu', symbol:'\$' ,decimalDigits: 3, customPattern: '\u00a4 #,###.#').format(state.latestPrice)), style: Theme.of(context).textTheme.headline5,);


                            return Container();
                          }
                      );
                    }


                    return Text('Loading ...');
                  },
              ),

            ],
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            children:  [
              BlocBuilder<CryptoExchangeSubscriptionBoxBloc, CryptoExchangeSubscriptionBoxState>(
                  builder: (context, state){
                    if(state is CryptoExchangeSubscriptionBoxLoadedState){
                      return Text('Updated: '+state.lastUpdatedTime);
                    }
                    return Text('Loading ...');
                  },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
