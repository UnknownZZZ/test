import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:cryptox/pages/home_page/home_page.dart';
import 'package:flutter/material.dart';

import 'config/themes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return AdaptiveTheme(
      initial: AdaptiveThemeMode.light,
      light: kLightTheme,
      dark: kDarkTheme,
      builder: (light,dark){
        return MaterialApp(

          title: 'CryptoX monitor',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,

          ),
          home: HomePage(),
        );
      },
    );
  }
}
