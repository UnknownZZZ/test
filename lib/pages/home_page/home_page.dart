import 'package:cryptox/blocs/HomePage/home_page_bloc.dart';
import 'package:cryptox/blocs/elements/crypto_exchange_subscription/crypto_exchange_subscription_box_bloc.dart';
import 'package:cryptox/domain/entities/crypto_historical_price.dart';
import 'package:cryptox/elements/chart_switcher_button.dart';
import 'package:cryptox/elements/crypto_exchange_subscription_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/io.dart';
import 'package:cryptox/config/constants.dart' as constants;
class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);
  final RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(const Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
    _refreshController.refreshToIdle();
  }


  @override
  Widget build(BuildContext context) {

    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => HomePageBloc()..add(const HomePageStartEvent(currencyPair: 'BTC/USD')),
        ),
        BlocProvider(
          create: (context) => CryptoExchangeSubscriptionBoxBloc()..add(const CryptoExchangeSubscriptionBoxStartEvent(currencyPair: 'BTC/USD')),
        ),
      ],

      child: Scaffold(
        body: SafeArea(
          child: SmartRefresher(
            enablePullDown: true,
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: () async{
              //_refreshController.loadComplete();
              _refreshController.loadNoData();
              //print('no data');
            },
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Welcome',style: Theme.of(context).textTheme.headline6?.merge(TextStyle(color: Color(0xffb3b3b3)))),
                            Text('Dear test user!',style: Theme.of(context).textTheme.headline5?.merge(TextStyle(color: Color(0xff0e263e)))),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.all(5),
                          decoration: const BoxDecoration(
                              color: Color(0xfff1f1f1),
                              shape: BoxShape.circle
                          ),
                          child: Icon(Icons.person),
                        ),

                      ],
                    ),

                    const SizedBox(height: 20),

                    const SizedBox(height: 20),
                    CryptoExchangeSubscriptionBox(),

                    const SizedBox(height: 20),
                    Container(
                      width: double.infinity,

                      padding: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: const Color(0xFFf1f1f1),
                      ),
                      child: Column(
                        children: [
                          BlocBuilder<HomePageBloc, HomePageState>(
                            builder: (context,state){

                              if(state is HomePageLoadedState){
                                return SizedBox(
                                  width: double.infinity,
                                  height: 200,
                                  child: charts.TimeSeriesChart(
                                    [
                                      charts.Series<CryptoHistoricalPrice, DateTime>(
                                        id: 'Price',
                                        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
                                        domainFn: (CryptoHistoricalPrice price, _) => price.time,
                                        measureFn: (CryptoHistoricalPrice price, _) => price.Price,
                                        data: state.cryptoHistoricalPriceList,

                                      )
                                    ],
                                    animate: true,
                                    defaultRenderer: charts.LineRendererConfig(
                                        includePoints: true,
                                    ),
                                    // Optionally pass in a [DateTimeFactory] used by the chart. The factory
                                    // should create the same type of [DateTime] as the data provided. If none
                                    // specified, the default creates local date time.
                                    dateTimeFactory: const charts.LocalDateTimeFactory(),

                                  ),
                                );
                              }
                              return Container(
                                child: Text('Loading ...'),
                              );
                            },
                          )

                        ],
                      ),
                    ),
                    const SizedBox(height: 20),
                    Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: ChartSwitcherButton(
                            title: 'Year',
                            active: false,
                          ),
                        ),
                        Expanded(
                          child: ChartSwitcherButton(
                            title: 'Month',
                            active: false,
                          ),
                        ),

                        Expanded(
                          child: ChartSwitcherButton(
                            title: 'Week',
                            active: true,
                          ),
                        ),
                        Expanded(
                            child:ChartSwitcherButton(
                              title: 'Day',
                              active: false,
                            ),
                        ),


                      ],
                    ),

                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}


