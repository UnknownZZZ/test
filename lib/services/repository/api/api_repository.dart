
import 'package:cryptox/services/repository/api/exchange_api_provider.dart';

class ApiRepository{
  final ExchangeApiProvider _exchangeApiProvider = ExchangeApiProvider();

  getExchangeRate({base,quote}) => _exchangeApiProvider.getExchangeRate(base,quote);
  getExchangeRateHistorical({base,quote}) => _exchangeApiProvider.getExchangeRateHistorical(base,quote);
  liveSubscription({base,quote, webSocketChannel}) => _exchangeApiProvider.liveSubscription(base,quote, webSocketChannel);

}
