import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:cryptox/config/constants.dart' as constants;
import 'package:intl/intl.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class ExchangeApiProvider{

  Future getExchangeRate(String base, String quote) async{
    final response = await http.get(
      Uri.parse(constants.apiBaseURL+"exchangerate/"+base+"/"+quote),
      headers: {
        'X-CoinAPI-Key':constants.apiKey,
      },

    );

    if(response.body.isNotEmpty){
      final  apiResult = jsonDecode(response.body)['rate'];
      return apiResult;
    }
    return "undefined error";
  }


  Future getExchangeRateHistorical(String base, String quote) async{

    final response = await http.get(
      Uri.parse(constants.apiBaseURL+"exchangerate/"+base+"/"+quote+"/history?period_id=1DAY&time_start=${DateFormat('yyyy-MM-dd').format(DateTime.now().subtract(Duration(days: 7)))}T00:00:00&time_end=${DateFormat('yyyy-MM-dd').format(DateTime.now())}T00:00:00"),

      headers: {
        'X-CoinAPI-Key':constants.apiKey,
      },
    );
    if(response.body.isNotEmpty){
      final  apiResult = jsonDecode(response.body);
      return apiResult;
    }
    return "undefined error";
  }

  Future liveSubscription(String base, String quote, WebSocketChannel webSocketChannel) async{

    webSocketChannel.sink.add(
        json.encode(
            {
              "type": "hello",
              "apikey": constants.apiKey,
              "heartbeat": false,
              "subscribe_data_type": ["trade"],
              "subscribe_filter_asset_id": ["${base}/${quote}"]
            }
        )

    );
  }
}